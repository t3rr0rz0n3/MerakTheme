��                        B        P     j     �     �     �  ,   �  $   �               *     3  �   8  :   �  R     )   r     �     �     �  I   �     0     7  
   F     Q  ,   V     �     �  ,   �  ,   �     �  �  �  B   }	     �	     �	     �	     	
     
  ,   3
  $   `
     �
     �
     �
     �
  �   �
  :   T  R   �  )   �          (     @  I   W     �     �  
   �     �  ,   �     �     �  ,     ,   .     [   Activa este efecto para que carguen las entradas automáticamente. Activar "Infinite Scroll" Activar "Lo más visto" Activar "Subir arriba" Activar Breadcrumbs Activar aviso cookies Activar breadcrumbs para saber donde estás. Activar compartir con redes sociales Agua Alpheratz Theme Amarillo Azul Añade al final de cada entrada la opción de compartir con redes sociales (Facebook, Twitter, Google+ y Email). ATENCIÓN: Está en fase alpha, no funciona del todo bien. Añade enlace en el footer para subir arriba (con efecto). Añade una lista en la parte superior de la entrada de los 3 posts más visitados. Cambia el Glyphicon del titulo principal. Cambia el estilo principal. Configuracion Avanzada Configuración básica En el menu principal nuestra el titulo principal del blog en dos colores. Estilo Fondo del tema Glyphicons Lila Muestra un aviso de las cookies (no molesto) Naranja Rojo Título principal de la web: Primera palabra Título principal de la web: Segunda palabra Verde Project-Id-Version: Alpheratz Theme
Report-Msgid-Bugs-To: 
POT-Creation-Date: Sat Jun 18 2016 20:00:02 GMT+0200 (CEST)
PO-Revision-Date: Sat Jun 18 2016 22:34:32 GMT+0200 (CEST)
Last-Translator: zagur <ffaefeafefae@ejemplo.com>
Language-Team: 
Language: Spanish (Spain)
Plural-Forms: nplurals=2; plural=n != 1
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: ..
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Generator: Loco - https://localise.biz/
X-Loco-Target-Locale: es_ES Activa este efecto para que carguen las entradas automáticamente. Activar "Infinite Scroll" Activar "Lo más visto" Activar "Subir arriba" Activar Breadcrumbs Activar aviso cookies Activar breadcrumbs para saber donde estás. Activar compartir con redes sociales Agua Alpheratz Theme Amarillo Azul Añade al final de cada entrada la opción de compartir con redes sociales (Facebook, Twitter, Google+ y Email). ATENCIÓN: Está en fase alpha, no funciona del todo bien. Añade enlace en el footer para subir arriba (con efecto). Añade una lista en la parte superior de la entrada de los 3 posts más visitados. Cambia el Glyphicon del titulo principal. Cambia el estilo principal. Configuración Avanzada Configuración básica En el menu principal nuestra el titulo principal del blog en dos colores. Estilo Fondo del tema Glyphicons Lila Muestra un aviso de las cookies (no molesto) Naranja Rojo Título principal de la web: Primera palabra Título principal de la web: Segunda palabra Verde 