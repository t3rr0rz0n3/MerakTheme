                <?php get_header(); ?>
                <div class="background-header-single">
                    <h1 class="page-title text-center">
                        <?php single_cat_title( $prefix = '', $display = true ); ?>
                    </h1>
                </div>

                <div class="content-archive">
                    <div id="primary">
                        <div class="container">

                            <div class="breadcrumbs">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php nc_breadcrumb(); ?>
                                    </div>
                                </div>
                            </div><!-- .breadcrumbs -->

                            <main id="main">
                                <div class="row">
                                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                                    <?php
                                        get_template_part( 'content-archive', get_post_format() );
                                    ?>

                                    <?php endwhile; else: ?>

                                    <div class="">
                                        <h1>
                                            <?php _e('Aún no hay artículos para cargar', 'MerakTheme'); ?>
                                        </h1>
                                    </div>

                                    <?php endif; ?>

                                    <?php comments_template('',true); ?>
                                </div>
                            </main>
                        </div><!-- .container -->
                    </div><!-- #primary -->
                </div><!-- .content-archive -->
                <?php get_footer(); ?>
