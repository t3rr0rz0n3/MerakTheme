                <?php get_header(); ?>
                <div class="content-home">
                    <div id="primary" class="home">
                        <main id="main">
                            <?php get_template_part( 'content');?>

                            <nav class="navigation paging-navigation" role="navigation">
                                <div class="nav-links">
                                    <?php //AlpheratzPagination(); ?>
                                <div><!-- .nav-links -->
                            </nav><!-- .navigation -->
                        </main>
                    </div><!-- #primary -->
                </div><!-- .container-fluid -->
                <?php get_footer(); ?>
