<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-6'); ?>>
    <div class="blog-item-wrap square">
        <div class="thumb">
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" >
                <div class="featured-background" height="300px" style="background-image: url(<?php echo get_thumbnail_post(); ?>)"></div>
                <div class="blog-info">
                    <h2 class="entry-title">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" >
                           <?php the_title(); ?>
                       </a>
				    </h2><!-- .entry-title -->
                </div>
            </a>
        </div><!-- .thumb -->
    </div><!-- square -->
</article><!-- #post-## -->
