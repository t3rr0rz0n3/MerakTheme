<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-12 new-post'); ?>>
    <div class="col-md-6">
        <div class="blog-item-wrap square">
            <div class="thumb">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" >
                    <div class="featured-background" height="300px" style="background-image: url(<?php echo get_thumbnail_post(); ?>)"></div>
                </a>
            </div><!-- .thumb -->
        </div><!-- square -->
    </div>
    <div class="col-md-6">
        <h2 class="entry-title">
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" >
               <?php the_title(); ?>
           </a>
        </h2><!-- .entry-title -->
        <ul class="info">
            <li>
                <?php the_time('F d, Y') ?>
            </li>
            <li>
                <span class="fa fa-tags"></span>
                <?php
                    $category = get_the_category();
                    if ($category) {
                      echo '<a href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "Ver todas las entradas de %s" ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> ';
                  }
                ?>
            </li>
        </ul><!-- .info -->
        <p class="post-resume">
            <?php echo getExcerpt(650) . ' [...]'; ?>
        </p>
    </div>
</article><!-- #post-## -->
