                <?php get_header(); ?>

                <div class="background-header-single">
					<h1 class="page-title entry-title text-center">
						<?php printf( __( 'Resultados para: %s', 'MerakTheme' ), '<span>' . get_search_query() . '</span>' ); ?>
					</h1>
                </div><!-- .background-header-single -->

                <div class="content-archive">
                    <div id="primary">
                        <div class="container">

                            <div class="breadcrumbs">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php nc_breadcrumb(); ?>
                                    </div>
                                </div>
                            </div><!-- .breadcrumbs -->

                            <main id="main">
                                <div class="row">

                                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                                    <?php
                                        get_template_part( 'content-search', get_post_format() );
                                    ?>

                                    <?php endwhile; else: ?>

                                    <div class="">
                                        <h1>
                                            <?php _e('Aún no hay artículos para cargar', 'AlpheratzTheme'); ?>
                                        </h1>
                                    </div>
                                    <?php endif; ?>
                                    <?php comments_template('',true); ?>
                                </div><!-- .row -->
                            </main>
                        </div><!-- .container -->
                    </div><!-- #primary -->
                </div><!-- .content-archive -->
                <?php get_footer(); ?>
