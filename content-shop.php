<article <?php post_class('col-md-6'); ?>>
    <div class="square-product">
        <div class="thumb">
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" >
                <div class="featured-background" height="300px" style="background-image: url(<?php echo get_thumbnail_post(); ?>)"></div>
                <div class="blog-info">
                    <h3 class="entry-title">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" >
                           <?php the_title(); ?>
                       </a>
                    </h3><!-- .entry-title -->
                </div>
            </a>
        </div><!-- .thumb -->
        <div class="square-product-info">
            <?php the_content(); ?>
            Price : $<?php echo get_post_meta($post->ID, "_regular_price", $single = true); ?>
            <?php
                $category = get_the_category();
                if ($category) {
                  echo '<a class="btn btn-default btn-merak-category" type="submit" role="button" href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "Ver todas las entradas de %s" ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> ';
                }
            ?>
            <?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
        </div>
    </div>
</article>
