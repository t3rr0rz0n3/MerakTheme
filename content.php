<!-- Carga el contenido de header, blog y portofolio -->

<?php if( of_get_option('typeweb') == "shop" ): ?>
<div class="header-shop">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-1">
				<h1>Diseño web libre</h1>
				<ul>
					<li>HTML</li>
					<li>SEO</li>
					<li>RESPONSIVE</li>
				</ul>
				<button class="btn btn-merak-category" type="button" name="button">Tienda</button>
				<button class="btn btn-merak-category" type="button" name="button">A la carta</button>
			</div>
		</div>
	</div>
</div>

<div class="zone-info-shop">
	<div class="container">
		<div class="row">
			<div class="col-md-4 info-shop-square">
				<div class="col-md-3">
					<div class="info-shop-circle">
						<i class="fa fa-desktop" aria-hidden="true"></i>
					</div>
				</div>
				<div class="col-md-9">
					<ul>
						<li>Item</li>
						<li>Item</li>
						<li>Item</li>
					</ul>
				</div>
			</div>
			<div class="col-md-4 info-shop-square">
				<div class="col-md-3">
					IMG
				</div>
				<div class="col-md-9">
					<ul>
						<li>Item</li>
						<li>Item</li>
						<li>Item</li>
					</ul>
				</div>
			</div>
			<div class="col-md-4 info-shop-square">
				<div class="col-md-3">
					IMG
				</div>
				<div class="col-md-9">
					<ul>
						<li>Item</li>
						<li>Item</li>
						<li>Item</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>


<?php if( of_get_option('typeweb') == "blog" ): ?>

<div class="zone-blog">
	<div class="container">
		<div class="row">
			<?php //query_posts('cat=-2&posts_per_page=4'); ?> <!-- categoria que no queremos mostrar -->

			<?php $count = 0; ?>
            <?php while (have_posts()) : the_post(); ?>
                <?php $count++; ?>
                <?php if ($count == 1) : ?>
	                <div class="row">
						<?php
		                    get_template_part( 'content-blog-first');
		                ?>
	                </div>
				<?php else : ?>
                    <?php
                        get_template_part( 'content-blog');
                    ?>
                <?php endif; ?>
            <?php endwhile;?>

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<?php
					get_template_part( 'content-blog');
				?>

			<?php endwhile; else: ?>

				<div class="">
					<h1>
						<?php _e('Aún no hay artículos para cargar', 'AlpheratzTheme'); ?>
					</h1>
				</div>

			<?php endif; ?>
		</div><!-- .col-md-8 -->
	</div><!-- .row -->
</div><!-- .zone-blog -->

<?php endif; ?>

<?php if( of_get_option('typeweb') == "shofp" ): ?> <!-- en teoria muestra articulos de la shop -->

<div class="zone-shop">
	<div class="container">
		<div class="row">
			<?php //query_posts('cat=-2&posts_per_page=4'); ?> <!-- categoria que no queremos mostrar -->
			
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<?php
					get_template_part( 'content-shop');
				?>

			<?php endwhile; else: ?>

				<div class="">
					<h1>
						<?php _e('Aún no hay artículos para cargar', 'AlpheratzTheme'); ?>
					</h1>
				</div>

			<?php endif; ?>
		</div><!-- .col-md-8 -->
	</div><!-- .row -->
</div><!-- .zone-blog -->

<?php endif; ?>

<?php if( of_get_option('typeweb') == "shop" ): ?>

<div class="zone-portfolio">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="text-center">Portofolio</h2>
				<span class="separator"></span>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae eum molestiae doloremque magnam natus, mollitia neque eos, quo nihil delectus id porro quidem odit laboriosam soluta assumenda officiis perspiciatis modi.
				</p>
				<div class="portfolio-menu">
					<ul id="filter-list" class="clearfix">
						<?php $cat_ID = get_all_category_ids(); ?>
						<?php $args = array(
							'orderby'	 => 'name',
							'order' 	 => 'DESC',
							'show_count' => '0',
							'class'		 => 'filter',
							'child_of'	 => '7' /* categoria padre portofolio */
						);
						$categories = get_categories( $args );
							echo '<li class="filter" data-filter="all">Todo</li>';
						foreach ( $categories as $category ) {
							echo '<li class="filter" data-filter="category-' . $category->name . '">' . $category->name . '</li>';
						}

						?>
					<ul><!-- #filter-item -->
				</div><!-- .portfolio-menu -->
			</div><!-- .col-md-12 -->
		</div><!-- .row -->
	</div><!-- .container -->

	<div class="container-fluid">
		<div class="row no-gutter">
			<div class="col-md-12">
				<ul id="portfolio">
					<?php query_posts('cat=7&posts_per_page=9'); ?>
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content-portofolio'); ?>

					<?php endwhile; else: ?>
						<div class="">
							<h1>
								<?php _e('Aún no hay artículos para cargar', 'AlpheratzTheme'); ?>
							</h1>
						</div>
					<?php endif; ?>
				</ul><!-- @end #portfolio -->
			</div>
		</div>
	</div>

</div><!-- .zone-portfolio -->

<div class="zone-shop">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			<h2 class="text-center">Shop</h2>
			<span class="separator"></span>
			<?php
				$args = array(
					'post_type' => 'product',
					'stock' => 1,
					'posts_per_page' => 4,
					'orderby' =>'date',
					'order' => 'ASC'
				);
				$loop = new WP_Query( $args );
			?>

			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

			<?php
				get_template_part('wocommerce/content-product');
			?>

			<?php endwhile; //wp_pagenavi(); ?>
			</div>
		</div>
	</div>
</div><!-- .zone-shop -->

<div class="zone-divider">
	<div class="" style="background-color: #454545; heigth: 50px;">
		divider
	</div>
</div>

<div class="zone-alacarta">
	form
</div>
<?php endif; ?>
