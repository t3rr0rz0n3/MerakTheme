<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-header">
		<div class="">
			<div class="col-md-2">
				<div class="avatar-img">
					<?php echo get_avatar(get_the_author_meta('ID') , '60'); ?>
				</div><!-- .author-img -->
			</div>
			<div class="col-md-10">
				<h2 class="title-post"><?php the_title(); ?></h2>
				<ul class="info">
		            <li>
		                <?php the_time('F d, Y') ?>
					</li>
		            <li>
		                <span class="fa fa-tags"></span>
		                <?php
		                    $category = get_the_category();
		                    if ($category) {
		                      echo '<a href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "Ver todas las entradas de %s" ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> ';
		                  }
		                ?>
		            </li>
					<li>
						<span class="fa fa-user"></span>
		                <a title="<?php the_author(); ?>" alt="<?php the_author(); ?>" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author(); ?></a>
		            </li>
		        </ul><!-- .info -->
			</div>
		</div><!-- .row -->

		<?php edit_post_link( __( 'Editar', 'MerakTheme' ), '<div class="edit-link"><span class="glyphicon glyphicon-edit"></span> <span class="edit-link">', '</span></div>' ); ?>

	</div><!-- .entry-header -->

	<div class="entry-post">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<?php the_content(); ?>
			</div>
		</div>
	</div><!-- .entry-post -->

	<div class="share-content-post">
		<?php nc_shareContent(); ?>
	</div>

	<div class="entry-post-info">
		<div class="row">
			<div class="col-md-5 col-md-offset-2">
				<?php if(has_tag()) : ?>
				<div class="tagcloud">
	                <span class="glyphicon glyphicon-tags"></span>
	                <?php
	                    $tags = get_the_tags(get_the_ID());
	                    foreach($tags as $tag){
	                        echo '<a href="'.get_tag_link($tag->term_id).'">'.$tag->name.'</a> ';
	                    }
	                ?>
	            </div><!-- .tagcloud -->
				<?php endif; ?>
			</div>
			<div class="col-md-5">
				<div class="license">
					<span class="text-license"><?php _e('Artículo bajo licencia: '); ?></span>
					<?php nc_license(); ?>
				</div>
			</div>
		</div>
	</div><!-- .entry-post-info -->

	<hr class="small">

	<div class="entry-related-post">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<h3><?php _e('Quizás te interesa esto'); ?></h3>
				<?php nc_relatedPosts(); ?>
			</div>
		</div>
	</div>

	<hr class="small">

</article><!-- #post-## -->

<?php if (is_single() ) { ?>
	<div class="single-author-nav hidden">
		<div class="row">
			<div class="show-first">

				<div class="col-md-1 text-left">
					<div class="avatar-img">
						<?php echo get_avatar(get_the_author_meta('ID') , '100'); ?>
					</div><!-- .author-img -->
				</div>

				<div class="col-md-3 text-left ">
					<div class="info">
						<span><?php echo get_the_author_meta('first_name') . ' ' . get_the_author_meta('last_name'); ?></span><br>
						<span class="date"><?php the_time('j M Y'); ?></span>
					</div>
				</div>

				<div class="col-md-4 text-center">
					<?php nc_toTopPage(); ?>
				</div>

				<div class="col-md-4 text-right">
					<div class="share-content-content">
						<i class="fa fa-bookmark-o"></i>
						<a class="open-panel-share"><?php _e('Compartir', 'MerakTheme'); ?></a>
					</div>
				</div>
			</div><!-- .show-first -->

			<div class="show-second hidden">
				<div class="col-md-4 text-left">
					<?php
						$next_post = get_next_post();
						if (!empty( $next_post )): ?>
							<a class="next-link" href="<?php echo get_permalink( $next_post->ID ); ?>">
								<i class="fa fa-angle-left" aria-hidden="true"></i>
								<div class="info">
									<span><?php _e("entrada anterior", 'MerakTheme'); ?></span><br>
									<span class="title"><?php echo $next_post->post_title; ?></span>
								</div>
							</a>
					<?php endif; ?>
				</div>

				<div class="col-md-4 text-center">
					<?php nc_toTopPage(); ?>
				</div>

				<div class="col-md-4 text-right">
					<?php
						$previous_post = get_previous_post();
						if (!empty( $previous_post )): ?>
							<a class="previous-link" href="<?php echo get_permalink( $previous_post->ID ); ?>">
								<i class="fa fa-angle-right" aria-hidden="true"></i>
								<div class="info">
									<span><?php _e("entrada anterior", 'MerakTheme'); ?></span><br>
									<span class="title"><?php echo $previous_post->post_title; ?></span>
								</div>
							</a>
					<?php endif; ?>
				</div>
			</div><!-- show-second -->
		</div><!-- .row -->
	</div><!-- .single-author-nav -->
<?php } ?>
