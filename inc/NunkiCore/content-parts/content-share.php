<div class="row">
    <div class="share-post text-right">
        <button type="button" class="close">×</button>
        <ul class="share-buttons">
            <!-- FACEBOOK -->
            <li class="fb">
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>&t=<?php the_title(); ?>" title="<?php _e('Compartir con Facebook', 'AlpheratzTheme'); ?>" target="_blank">
                     <i class="fa fa-facebook"></i>
                </a>
            </li>
            <!-- TWITTER -->
            <li class="tw">
                <a href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>&text=<?php the_title(); ?>%20%23PortalLinux&via=zagurito" target="_blank" title="<?php _e('Compartir con Twitter', 'AlpheratzTheme'); ?>">
                    <i class="fa fa-twitter"></i>
                </a>
            </li>
            <!-- Google+ -->
            <li class="gp">
                <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank" title="<?php _e('¡Compartir en Google+!', 'AlpheratzTheme'); ?>">
                     <i class="fa fa-google-plus"></i>
                </a>
            </li>
            <!-- Email -->
            <li class="em">
                <a href="mailto:?subject=<?php the_title(); ?>&body=<?php the_title(); ?>:<?php the_permalink(); ?>" target="_blank" title="<?php _e('Envia artículo por correo', 'AlpheratzTheme'); ?>">
                    <i class="fa fa-envelope-o"></i>
                </a>
            </li>
            <li class="close">
                <a>
                    <i class="fa fa-times"></i>
                </a>
            </li>
        </ul><!-- .share-buttons -->
    </div> <!-- /. share-post -->
</div><!-- .row -->
