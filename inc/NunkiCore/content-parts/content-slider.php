<div id="MySlide" class="carousel slide visible-lg" data-ride="carousel" data-pause="<?php echo nc_pauseOnHover(); ?>" data-interval="<?php echo of_get_option('interval', '2000') ?>">

    <!-- Indicators -->
    <ol class="carousel-indicators">
        <?php
            $numpost = of_get_option('numpost');
            for ($j = 0; $j < $numpost; $j++) {
                if ($j == 0) {
                    echo '<li data-target="#MySlide" data-slide-to="0" class="active"></li>';
                } else {
                    echo '<li data-target="#MySlide" data-slide-to="' . $j .'"></li>';
                }
            }
        ?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <?php
            $i=1;
            $the_query = new WP_Query(array(‘category_name’ => of_get_option('categoriaslider', 'uncategorized'), ‘posts_per_page’ => of_get_option('numpost') ));

            while ( $the_query->have_posts() ) : $the_query->the_post();

                if ($i == 1) {  ?>
                    <div class="item active">
                        <?php the_post_thumbnail('large', array( 'class' => 'img-responsive' )); ?>
                        <div class="carousel-caption">
                            <h1><?php the_title(); ?></h1>
                            <p><?php the_excerpt(); ?></p>
                        </div>
                    </div>
                    <?php }
                else if ($i > 1 && $i <= of_get_option('numpost')) { ?>
                <div class="item">
                    <?php the_post_thumbnail('large', array( 'class' => 'img-responsive' )); ?>
                    <div class="carousel-caption">
                        <h1><?php the_title(); ?></h1>
                        <p><?php the_excerpt(); ?></p>
                    </div>
                </div><!-- item -->
            <?php }
            $i++; endwhile;
            wp_reset_postdata();
        ?>
    </div><!-- carousel-inner -->





    <!-- Controls -->
    <a class="left carousel-control" href="#MySlide" role="button" data-slide="prev">
        <span class="fa fa-angle-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#MySlide" role="button" data-slide="next">
        <span class="fa fa-angle-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
