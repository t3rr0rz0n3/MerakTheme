<?php

/* 			SET OPTIONS TO DISPLAY THE PANEL NUNKI CORE				*/

/* GENERAL SETTINGS */
$TypeFormatWeb = 1;
$ThemeStylesheet = 1;
$Favicon = 1;
$TrackingCode = 1;
$LayoutStyle = 1;
$Ads = 1;
$CookiesNotice = 1;
$BreadCrumb = 1;
$ToTopEffect = 1;
$RandomPosts = 1;
$CollaborateMessage = 1;
$RelatedPosts = 1;
$ShareContent = 1;

/* SECTIONS */
$SectionsHome = 1;

/* HEADER */
$HeaderBackground = 1;
$HeaderGlyphicons = 1;
$HeaderBrand = 1;

/* SLIDER */
$Slider = 1;

/* TYPOGRAPHY */

/* INFINITE SCROLL */
$InfiniteScroll = 1;

/* SOCIAL OPTIONS */
$SocialOptions = 1;

/* LICENSE */
$SelectLicense = 1;

/* FOOTER OPTIONS */
$FooterOptions = 1;

/* THEME UPDATE */
$ThemeUpdate = 1;

/* OTHERS */
$ChristmassTree = 1;
$CSSExtra = 1;
$JSExtra = 1;

?>
