                <?php get_header(); ?>

                <div class="image-header-single">
                    <h1 class="page-title text-center">
                        <?php the_title(); ?>
                    </h1>
                </div><!-- .image-header-single -->

                <div class="content-page">
                    <div id="primary">
                        <div class="container">

                            <div class="breadcrumbs">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php nc_breadcrumb(); ?>
                                    </div>
                                </div>
                            </div><!-- .breadcrumbs -->

                            <main id="main">
                                <div class="row">
                                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                                        <?php get_template_part( 'content-page' ); ?>

                                    <?php endwhile; else: ?>

                                        <?php get_template_part( '404'); ?>

                                    <?php endif; ?>
                                </div>
                            </main>
                        </div>
                    </div><!-- #primary -->
                </div><!-- post-content -->
                <?php get_footer(); ?>
