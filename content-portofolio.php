<li <?php post_class('item col-md-3'); ?>>
    <div class="featured-background" height="300px" style="background-image: url(<?php echo get_thumbnail_post(); ?>)"></div>
    <ul class="caption list-none">
        <li>
            <a href="#" data-toggle="modal" data-target="#myModal-<?php the_ID(); ?>">
                <i class="fa fa-eye" aria-hidden="true"></i>
            </a>
        </li>
        <li>
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" >
                <i class="fa fa-link" aria-hidden="true"></i>
            </a>
        </li>
    </ul>
</li>

<!-- Modal -->
<div class="modal fade" id="myModal-<?php the_ID(); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php the_title() ?></h4>
            </div><!-- .modal-content -->

            <div class="modal-body">
                <img src="<?php echo get_post_meta($post->ID, 'img_modal_portofolio', true); ?>" alt="" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-merak-category" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-merak-category">Save changes</button>
            </div><!-- .moda-footer -->
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div>
