                <?php get_header(); ?>

                <div class="image-header-single">
                    <div class="image-header" style="background-image: url('<?php echo getImageHeader(); ?>');"></div>
                </div><!-- .image-header-single -->

                <div class="content-single">
                    <div id="primary">
                        <div class="container">

                            <div class="breadcrumbs">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php nc_breadcrumb(); ?>
                                    </div>
                                </div>
                            </div><!-- .breadcrumbs -->

                            <main id="main">
                                <div class="row">
                                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                                    <?php get_template_part( 'content-single', get_post_format() );?>

                                    <?php endwhile; else: ?>

                                        <div class="">
                                            <h1>
                                                <?php _e('Aún no hay artículos para cargar', 'AlpheratzTheme'); ?>
                                            </h1>
                                        </div>

                                    <?php endif; ?>
                                    <?php comments_template('',true); ?>
                                </div>
                            </main>
                        </div>
                    </div><!-- #primary -->
                </div><!-- post-content -->
                <?php get_footer(); ?>
