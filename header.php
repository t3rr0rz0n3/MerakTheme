<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="keywords" content="Software Libre, Stallman, Linux, GNU, GNU/Linux">
        <meta name="author" content="Jesús Camacho">
        <meta name="description" content="<?php bloginfo('description'); ?>">
        <title>
            <?php
                if (is_home()) {
                    bloginfo('name');
                    echo ": ";
                    bloginfo('description');
                } else {
                    bloginfo('name');
                    echo ": ";
                    wp_title();
                }
         ?>
        </title>

        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

        <!-- RSS -->
        <link rel="alternate" type="aplication/rss+xml" title="PortalLinux" href="http://www.portallinux.es/feed" />

        <!-- FAVICON -->
        <link rel="shortcut icon" href="<?php echo of_get_option( 'favicon' ); ?>" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <!-- Styles -->
        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ) ?>/css/normalize.min.css" media="screen" charset="utf-8">
        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ) ?>/css/bootstrap.min.css" media="screen" charset="utf-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ) ?>/css/cc-icons.css" media="screen" charset="utf-8">
        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/style.css" media="screen" charset="utf-8">
        <?php if( of_get_option('typeweb') == "shop" ): ?>
        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/shop.css" media="screen" charset="utf-8">
        <?php endif; ?>
        <?php if( of_get_option('typeweb') == "blog" ): ?>
        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/blog.css" media="screen" charset="utf-8">
        <?php endif; ?>

        <style media="screen">
            <?php echo of_get_option( 'cssextra' ); ?>
        </style>
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=News+Cycle" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lalezar" rel="stylesheet">


        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); echo nc_changeBkgImgOrColor(); ?>>
        <main id="page">
            <header>
                <nav class="navbar navbar-default navbar-fixed-top">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed buscador">
                            <a href="#searchBox" title="Buscar">
                                <span class="fa fa-search" aria-hidden="true"></span>
                            </a>
                        </button>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a  class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <span class="glyphicon <?php echo of_get_option( 'glyphicon', 'glyphicon glyphicon-flash' ); ?>"></span>
                            <span class="first"><strong><?php echo of_get_option('firstword', 'Merak'); ?></strong></span>
                            <span class="second"><strong><?php echo of_get_option('secondword', 'Theme'); ?></strong></span>
                        </a>
                    </div>

                        <?php
                           wp_nav_menu( array(
                                'menu'              => 'primary',
                                'theme_location'    => 'primary',
                                'depth'             => 2,
                                'container'         => 'div',
                                'container_class'   => 'collapse navbar-collapse',
                                'container_id'      => 'bs-example-navbar-collapse-1',
                                'menu_class'        => 'nav navbar-nav navbar-right',
                                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                                'walker'            => new wp_bootstrap_navwalker()
                            ));
                        ?>

                </nav>
            </header>

            <div id="content-full">
                <!-- BUSCADOR -->
        		<!--<div id="box_animate" class="col-md-12">
                    <div class="right-inner-addon">
               			<aside id="search" class="widget widget_search">
        					<?php //get_search_form(); ?>
        				</aside>
            		</div>
        		</div>--><!-- #box_animate -->

                <div class="main-content-area">
