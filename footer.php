                    </div><!-- .main-content-area -->
                </div><!-- #content-full -->
            <div class="panel-share hidden">
               <?php nc_shareContent(); ?>
               <div class="widgets-tests">
                   <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer') ) : endif; ?>
               </div>
            </div>
            <div id="footer-area">
                <div class="pre-footer">
                    <ul class="social-network list-inline">
                        <?php nc_SocialNetworkFooter(); ?>
                    </ul>
                </div>
                <div class="footer">
                    <div class="col-md-6 text-left">
                        <span class="glyphicon <?php echo of_get_option( 'glyphicon', 'glyphicon glyphicon-flash' ); ?>"></span>
                        <span class="first"><strong><?php echo of_get_option('firstword', 'Merak'); ?></strong></span>
                        <span class="second"><strong><?php echo of_get_option('secondword', 'Theme'); ?></strong></span>
                    </div>
                    <div class="col-md-6 text-right">
                        <span><a href="#">Merak Theme</a> by <a href="#">Jesús Camacho</a></span>
                    </div>
                </div>
            </div><!-- #footer-area -->
            <?php nc_christmasTree(); ?>
            <?php nc_cookiesMessage(); ?>
        </main><!-- #page -->

        <!-- Scripts -->
        <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/alpheratz.js" type="text/javascript"></script>
        <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/filterable.pack.js" type="text/javascript"></script>
        <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.mixitup.min.js" type="text/javascript"></script>
        <script type="text/javascript">
        			$(function(){
        				$('#portfolio').mixitup({
        				targetSelector: '.item',
        				transitionSpeed: 450
        				});
        			});
        		</script>

        <?php wp_footer(); ?>
    </body>
</html>
