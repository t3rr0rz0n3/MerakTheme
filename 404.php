<?php get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<header class="page-header">
				<h1 class="page-title"><?php _e( '¡Oh, no! 😮 ¡Maldición!', 'AlpheratzTheme' ); ?></h1>
			</header><!-- .page-header -->

			<div class="post-inner-content error-content">
				<div class="page-content">
					<p class="error404 text-center">
						(╯°□°）╯︵ ┻━┻
					</p>
					<h3 class="h3error text-center"><?php _e( '¡Estamos siendo invadidos por gente que lanza mesas y no podemos mostrar el contenido que buscabas! 😔', 'AlpheratzTheme') ?></h3>
					<h4 class="h4error text-center"><?php _e( 'Quizás este apuesto y seductor buscador te puede ayudar en algo... 😏', 'AlpheratzTheme' ); ?></h4>

					<?php get_search_form(); ?>

				</div><!-- .page-content -->
			</div><!-- .post-inner-content -->
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
