// SINGLE NAV POST
$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    var scrollFinal = $(window).height();

    var $win = $(window);

    if (scroll >= 40) {
        $('.single-author-nav').fadeIn('fast');
        $('.single-author-nav').removeClass('hidden');
    } else {
        $('.single-author-nav').fadeOut('fast');
    }

    if (scroll >= 500) {
        $('.show-first').fadeOut('fast');
        $('.show-second').fadeIn('fast');
        $('.show-second').removeClass('hidden');
    } else {
        $('.show-second').fadeOut('fast');
        $('.show-first').fadeIn('fast');
        $('.show-second').addClass('hidden');
    }

    if (scroll + scrollFinal == $(document).height()) {
        $('.single-author-nav').fadeOut('fast').addClass('hidden');
    } else {
        $('.single-author-nav').removeClass('hidden');
    }
});


$(document).ready(function(){
    var previousScroll = 0;

    $(window).scroll(function(){
        var currentScroll = $(this).scrollTop();

        if (currentScroll > 0 && currentScroll < $(document).height() - $(window).height()) {

            if (currentScroll > previousScroll){
                window.setTimeout(hideNav, 300);
            } else {
                window.setTimeout(showNav, 300);
            }
            previousScroll = currentScroll;
        }
    });
    function hideNav() {
        $("[data-nav-status='toggle']").removeClass("nav-is-visible").addClass("nav-is-hidden");
    }

    function showNav() {
        $("[data-nav-status='toggle']").removeClass("nav-is-hidden").addClass("nav-is-visible");
    }
});

$('.open-panel-share').click(function (){
    $('.panel-share').fadeIn('2000').removeClass('hidden');
});

$('.panel-share .close').click(function (){
    $('.panel-share').fadeOut('2000').AddClass('hidden');
});







// Close cookies
$(document).ready(function(){
    var nombreCookie="PortalLinux-cookie";

    if(getCookie(nombreCookie)==""){
        $('.cookies').removeClass("oculto");
        $('.close-capa').dblclick(function(){
            $(this).fadeOut(500);
            createCookie(nombreCookie,1,365);
        });
    };
});

function createCookie(name,value,days){
    if(days){
        var date=new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires="; expires="+date.toGMTString();
    }else
        var expires="";
        document.cookie=name+"="+value+expires+"; path=/";
}

function readCookie(name){
    var nameEQ=name+"=";
    var ca=document.cookie.split(';');

    for(var i=0;i<ca.length;i++){
        var c=ca[i];

        while (c.charAt(0) == ' ') {
            c = c.substring(1,c.length);
        }

        if (c.indexOf(nameEQ)==0){
            return c.substring(nameEQ.length,c.length);
        }
    }
    return null;
}

function eraseCookie(name){
    createCookie(name,"",-1);
}

function getCookie(cname){
    var name=cname+"=";
    var ca=document.cookie.split(';');

    for(var i=0;i<ca.length;i++){
        var c=ca[i];

        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }

        if (c.indexOf(name) == 0) {
            return c.substring(name.length,c.length);
        }
    }
    return"";
}
